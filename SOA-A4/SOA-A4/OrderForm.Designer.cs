﻿namespace SOA_A4
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbCustomer = new System.Windows.Forms.GroupBox();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labellastName = new System.Windows.Forms.Label();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelCustID = new System.Windows.Forms.Label();
            this.tbPhoneNumber = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbCustID = new System.Windows.Forms.TextBox();
            this.gbProduct = new System.Windows.Forms.GroupBox();
            this.prodWeight = new System.Windows.Forms.Label();
            this.tbProdID = new System.Windows.Forms.TextBox();
            this.labelPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.labelProdName = new System.Windows.Forms.Label();
            this.tbProdName = new System.Windows.Forms.TextBox();
            this.labelProdID = new System.Windows.Forms.Label();
            this.tbProdWeight = new System.Windows.Forms.TextBox();
            this.gbOrder = new System.Windows.Forms.GroupBox();
            this.labelOrderDate = new System.Windows.Forms.Label();
            this.tbOrderID = new System.Windows.Forms.TextBox();
            this.labelPoNumber = new System.Windows.Forms.Label();
            this.tbPoNumber = new System.Windows.Forms.TextBox();
            this.labelCustID2 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.labelOrderID = new System.Windows.Forms.Label();
            this.tbOrderDate = new System.Windows.Forms.TextBox();
            this.gbCart = new System.Windows.Forms.GroupBox();
            this.tbCartOderID = new System.Windows.Forms.TextBox();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.labelCartProdID = new System.Windows.Forms.Label();
            this.tbCartProdID = new System.Windows.Forms.TextBox();
            this.labelOrderID2 = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.gbGeneratePurchaseOrder = new System.Windows.Forms.GroupBox();
            this.cbPurchaseOrder = new System.Windows.Forms.CheckBox();
            this.gbCustomer.SuspendLayout();
            this.gbProduct.SuspendLayout();
            this.gbOrder.SuspendLayout();
            this.gbCart.SuspendLayout();
            this.gbGeneratePurchaseOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCustomer
            // 
            this.gbCustomer.Controls.Add(this.labelPhoneNumber);
            this.gbCustomer.Controls.Add(this.labellastName);
            this.gbCustomer.Controls.Add(this.labelFirstName);
            this.gbCustomer.Controls.Add(this.labelCustID);
            this.gbCustomer.Controls.Add(this.tbPhoneNumber);
            this.gbCustomer.Controls.Add(this.tbFirstName);
            this.gbCustomer.Controls.Add(this.tbLastName);
            this.gbCustomer.Controls.Add(this.tbCustID);
            this.gbCustomer.Location = new System.Drawing.Point(12, 123);
            this.gbCustomer.Name = "gbCustomer";
            this.gbCustomer.Size = new System.Drawing.Size(886, 97);
            this.gbCustomer.TabIndex = 0;
            this.gbCustomer.TabStop = false;
            this.gbCustomer.Text = "Customer";
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Location = new System.Drawing.Point(683, 45);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(74, 13);
            this.labelPhoneNumber.TabIndex = 7;
            this.labelPhoneNumber.Text = "phoneNumber";
            // 
            // labellastName
            // 
            this.labellastName.AutoSize = true;
            this.labellastName.Location = new System.Drawing.Point(483, 45);
            this.labellastName.Name = "labellastName";
            this.labellastName.Size = new System.Drawing.Size(51, 13);
            this.labellastName.TabIndex = 6;
            this.labellastName.Text = "lastName";
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(255, 45);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(51, 13);
            this.labelFirstName.TabIndex = 5;
            this.labelFirstName.Text = "firstName";
            // 
            // labelCustID
            // 
            this.labelCustID.AutoSize = true;
            this.labelCustID.Location = new System.Drawing.Point(52, 45);
            this.labelCustID.Name = "labelCustID";
            this.labelCustID.Size = new System.Drawing.Size(38, 13);
            this.labelCustID.TabIndex = 4;
            this.labelCustID.Text = "custID";
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Location = new System.Drawing.Point(758, 38);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(100, 20);
            this.tbPhoneNumber.TabIndex = 3;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(309, 38);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(100, 20);
            this.tbFirstName.TabIndex = 2;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(535, 38);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(100, 20);
            this.tbLastName.TabIndex = 1;
            // 
            // tbCustID
            // 
            this.tbCustID.Location = new System.Drawing.Point(93, 38);
            this.tbCustID.Name = "tbCustID";
            this.tbCustID.Size = new System.Drawing.Size(100, 20);
            this.tbCustID.TabIndex = 0;
            // 
            // gbProduct
            // 
            this.gbProduct.Controls.Add(this.prodWeight);
            this.gbProduct.Controls.Add(this.tbProdID);
            this.gbProduct.Controls.Add(this.labelPrice);
            this.gbProduct.Controls.Add(this.tbPrice);
            this.gbProduct.Controls.Add(this.labelProdName);
            this.gbProduct.Controls.Add(this.tbProdName);
            this.gbProduct.Controls.Add(this.labelProdID);
            this.gbProduct.Controls.Add(this.tbProdWeight);
            this.gbProduct.Location = new System.Drawing.Point(12, 241);
            this.gbProduct.Name = "gbProduct";
            this.gbProduct.Size = new System.Drawing.Size(886, 97);
            this.gbProduct.TabIndex = 1;
            this.gbProduct.TabStop = false;
            this.gbProduct.Text = "Product";
            // 
            // prodWeight
            // 
            this.prodWeight.AutoSize = true;
            this.prodWeight.Location = new System.Drawing.Point(695, 50);
            this.prodWeight.Name = "prodWeight";
            this.prodWeight.Size = new System.Drawing.Size(62, 13);
            this.prodWeight.TabIndex = 15;
            this.prodWeight.Text = "prodWeight";
            // 
            // tbProdID
            // 
            this.tbProdID.Location = new System.Drawing.Point(92, 43);
            this.tbProdID.Name = "tbProdID";
            this.tbProdID.Size = new System.Drawing.Size(100, 20);
            this.tbProdID.TabIndex = 8;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(493, 50);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(31, 13);
            this.labelPrice.TabIndex = 14;
            this.labelPrice.Text = "Price";
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(534, 43);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(100, 20);
            this.tbPrice.TabIndex = 9;
            // 
            // labelProdName
            // 
            this.labelProdName.AutoSize = true;
            this.labelProdName.Location = new System.Drawing.Point(252, 50);
            this.labelProdName.Name = "labelProdName";
            this.labelProdName.Size = new System.Drawing.Size(56, 13);
            this.labelProdName.TabIndex = 13;
            this.labelProdName.Text = "prodName";
            // 
            // tbProdName
            // 
            this.tbProdName.Location = new System.Drawing.Point(308, 43);
            this.tbProdName.Name = "tbProdName";
            this.tbProdName.Size = new System.Drawing.Size(100, 20);
            this.tbProdName.TabIndex = 10;
            // 
            // labelProdID
            // 
            this.labelProdID.AutoSize = true;
            this.labelProdID.Location = new System.Drawing.Point(51, 50);
            this.labelProdID.Name = "labelProdID";
            this.labelProdID.Size = new System.Drawing.Size(39, 13);
            this.labelProdID.TabIndex = 12;
            this.labelProdID.Text = "prodID";
            // 
            // tbProdWeight
            // 
            this.tbProdWeight.Location = new System.Drawing.Point(757, 43);
            this.tbProdWeight.Name = "tbProdWeight";
            this.tbProdWeight.Size = new System.Drawing.Size(100, 20);
            this.tbProdWeight.TabIndex = 11;
            // 
            // gbOrder
            // 
            this.gbOrder.Controls.Add(this.labelOrderDate);
            this.gbOrder.Controls.Add(this.tbOrderID);
            this.gbOrder.Controls.Add(this.labelPoNumber);
            this.gbOrder.Controls.Add(this.tbPoNumber);
            this.gbOrder.Controls.Add(this.labelCustID2);
            this.gbOrder.Controls.Add(this.textBox10);
            this.gbOrder.Controls.Add(this.labelOrderID);
            this.gbOrder.Controls.Add(this.tbOrderDate);
            this.gbOrder.Location = new System.Drawing.Point(12, 361);
            this.gbOrder.Name = "gbOrder";
            this.gbOrder.Size = new System.Drawing.Size(886, 97);
            this.gbOrder.TabIndex = 2;
            this.gbOrder.TabStop = false;
            this.gbOrder.Text = "Order";
            // 
            // labelOrderDate
            // 
            this.labelOrderDate.AutoSize = true;
            this.labelOrderDate.Location = new System.Drawing.Point(703, 48);
            this.labelOrderDate.Name = "labelOrderDate";
            this.labelOrderDate.Size = new System.Drawing.Size(54, 13);
            this.labelOrderDate.TabIndex = 23;
            this.labelOrderDate.Text = "orderDate";
            // 
            // tbOrderID
            // 
            this.tbOrderID.Location = new System.Drawing.Point(92, 41);
            this.tbOrderID.Name = "tbOrderID";
            this.tbOrderID.Size = new System.Drawing.Size(100, 20);
            this.tbOrderID.TabIndex = 16;
            // 
            // labelPoNumber
            // 
            this.labelPoNumber.AutoSize = true;
            this.labelPoNumber.Location = new System.Drawing.Point(478, 48);
            this.labelPoNumber.Name = "labelPoNumber";
            this.labelPoNumber.Size = new System.Drawing.Size(56, 13);
            this.labelPoNumber.TabIndex = 22;
            this.labelPoNumber.Text = "poNumber";
            // 
            // tbPoNumber
            // 
            this.tbPoNumber.Location = new System.Drawing.Point(534, 41);
            this.tbPoNumber.Name = "tbPoNumber";
            this.tbPoNumber.Size = new System.Drawing.Size(100, 20);
            this.tbPoNumber.TabIndex = 17;
            // 
            // labelCustID2
            // 
            this.labelCustID2.AutoSize = true;
            this.labelCustID2.Location = new System.Drawing.Point(267, 48);
            this.labelCustID2.Name = "labelCustID2";
            this.labelCustID2.Size = new System.Drawing.Size(38, 13);
            this.labelCustID2.TabIndex = 21;
            this.labelCustID2.Text = "custID";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(308, 41);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 18;
            // 
            // labelOrderID
            // 
            this.labelOrderID.AutoSize = true;
            this.labelOrderID.Location = new System.Drawing.Point(51, 48);
            this.labelOrderID.Name = "labelOrderID";
            this.labelOrderID.Size = new System.Drawing.Size(42, 13);
            this.labelOrderID.TabIndex = 20;
            this.labelOrderID.Text = "orderID";
            // 
            // tbOrderDate
            // 
            this.tbOrderDate.Location = new System.Drawing.Point(757, 41);
            this.tbOrderDate.Name = "tbOrderDate";
            this.tbOrderDate.Size = new System.Drawing.Size(100, 20);
            this.tbOrderDate.TabIndex = 19;
            // 
            // gbCart
            // 
            this.gbCart.Controls.Add(this.tbCartOderID);
            this.gbCart.Controls.Add(this.labelQuantity);
            this.gbCart.Controls.Add(this.textBox15);
            this.gbCart.Controls.Add(this.labelCartProdID);
            this.gbCart.Controls.Add(this.tbCartProdID);
            this.gbCart.Controls.Add(this.labelOrderID2);
            this.gbCart.Location = new System.Drawing.Point(12, 484);
            this.gbCart.Name = "gbCart";
            this.gbCart.Size = new System.Drawing.Size(886, 97);
            this.gbCart.TabIndex = 3;
            this.gbCart.TabStop = false;
            this.gbCart.Text = "Cart";
            // 
            // tbCartOderID
            // 
            this.tbCartOderID.Location = new System.Drawing.Point(92, 38);
            this.tbCartOderID.Name = "tbCartOderID";
            this.tbCartOderID.Size = new System.Drawing.Size(100, 20);
            this.tbCartOderID.TabIndex = 24;
            // 
            // labelQuantity
            // 
            this.labelQuantity.AutoSize = true;
            this.labelQuantity.Location = new System.Drawing.Point(490, 45);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(44, 13);
            this.labelQuantity.TabIndex = 30;
            this.labelQuantity.Text = "quantity";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(534, 38);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 25;
            // 
            // labelCartProdID
            // 
            this.labelCartProdID.AutoSize = true;
            this.labelCartProdID.Location = new System.Drawing.Point(267, 45);
            this.labelCartProdID.Name = "labelCartProdID";
            this.labelCartProdID.Size = new System.Drawing.Size(39, 13);
            this.labelCartProdID.TabIndex = 29;
            this.labelCartProdID.Text = "prodID";
            // 
            // tbCartProdID
            // 
            this.tbCartProdID.Location = new System.Drawing.Point(308, 38);
            this.tbCartProdID.Name = "tbCartProdID";
            this.tbCartProdID.Size = new System.Drawing.Size(100, 20);
            this.tbCartProdID.TabIndex = 26;
            // 
            // labelOrderID2
            // 
            this.labelOrderID2.AutoSize = true;
            this.labelOrderID2.Location = new System.Drawing.Point(51, 45);
            this.labelOrderID2.Name = "labelOrderID2";
            this.labelOrderID2.Size = new System.Drawing.Size(42, 13);
            this.labelOrderID2.TabIndex = 28;
            this.labelOrderID2.Text = "orderID";
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Wide Latin", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.Red;
            this.labelTitle.Location = new System.Drawing.Point(36, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(834, 26);
            this.labelTitle.TabIndex = 13;
            this.labelTitle.Text = "CRAZY MELVIN\'S SHOPPING EMPORIUM";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(104, 603);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(101, 55);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "Go Back";
            this.btnBack.UseVisualStyleBackColor = false;
            // 
            // btnExecute
            // 
            this.btnExecute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnExecute.ForeColor = System.Drawing.Color.White;
            this.btnExecute.Location = new System.Drawing.Point(392, 603);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(101, 55);
            this.btnExecute.TabIndex = 15;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(679, 603);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(101, 55);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "Get me outta here !";
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // gbGeneratePurchaseOrder
            // 
            this.gbGeneratePurchaseOrder.Controls.Add(this.cbPurchaseOrder);
            this.gbGeneratePurchaseOrder.Location = new System.Drawing.Point(270, 60);
            this.gbGeneratePurchaseOrder.Name = "gbGeneratePurchaseOrder";
            this.gbGeneratePurchaseOrder.Size = new System.Drawing.Size(306, 57);
            this.gbGeneratePurchaseOrder.TabIndex = 17;
            this.gbGeneratePurchaseOrder.TabStop = false;
            // 
            // cbPurchaseOrder
            // 
            this.cbPurchaseOrder.AutoSize = true;
            this.cbPurchaseOrder.Location = new System.Drawing.Point(40, 19);
            this.cbPurchaseOrder.Name = "cbPurchaseOrder";
            this.cbPurchaseOrder.Size = new System.Drawing.Size(219, 17);
            this.cbPurchaseOrder.TabIndex = 1;
            this.cbPurchaseOrder.Text = "Please generate a Purchase Order (P.O.)";
            this.cbPurchaseOrder.UseVisualStyleBackColor = true;
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 685);
            this.Controls.Add(this.gbGeneratePurchaseOrder);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.gbCart);
            this.Controls.Add(this.gbOrder);
            this.Controls.Add(this.gbProduct);
            this.Controls.Add(this.gbCustomer);
            this.Name = "OrderForm";
            this.Text = "OrderForm";
            this.gbCustomer.ResumeLayout(false);
            this.gbCustomer.PerformLayout();
            this.gbProduct.ResumeLayout(false);
            this.gbProduct.PerformLayout();
            this.gbOrder.ResumeLayout(false);
            this.gbOrder.PerformLayout();
            this.gbCart.ResumeLayout(false);
            this.gbCart.PerformLayout();
            this.gbGeneratePurchaseOrder.ResumeLayout(false);
            this.gbGeneratePurchaseOrder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCustomer;
        private System.Windows.Forms.GroupBox gbProduct;
        private System.Windows.Forms.GroupBox gbOrder;
        private System.Windows.Forms.GroupBox gbCart;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox gbGeneratePurchaseOrder;
        private System.Windows.Forms.CheckBox cbPurchaseOrder;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labellastName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.Label labelCustID;
        private System.Windows.Forms.TextBox tbPhoneNumber;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.TextBox tbCustID;
        private System.Windows.Forms.Label prodWeight;
        private System.Windows.Forms.TextBox tbProdID;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Label labelProdName;
        private System.Windows.Forms.TextBox tbProdName;
        private System.Windows.Forms.Label labelProdID;
        private System.Windows.Forms.TextBox tbProdWeight;
        private System.Windows.Forms.Label labelOrderDate;
        private System.Windows.Forms.TextBox tbOrderID;
        private System.Windows.Forms.Label labelPoNumber;
        private System.Windows.Forms.TextBox tbPoNumber;
        private System.Windows.Forms.Label labelCustID2;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label labelOrderID;
        private System.Windows.Forms.TextBox tbOrderDate;
        private System.Windows.Forms.TextBox tbCartOderID;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label labelCartProdID;
        private System.Windows.Forms.TextBox tbCartProdID;
        private System.Windows.Forms.Label labelOrderID2;
    }
}